#!/bin/bash
set -ex

CURRENTDIR="$(pwd)"
mkdir binout
apt update 
apt upgrade -y
curl -sL https://deb.nodesource.com/setup_current.x | bash - && apt -y install nodejs
mkdir -p  "$GOPATH/src/code.gitea.io/
cd  "$GOPATH/src/code.gitea.io/
git clone https://github.com/go-gitea/gitea.git
cd gitea

# check th last found
git checkout $(git for-each-ref refs/tags --sort=-taggerdate --format='%(refname)' --count=10 | grep -v  'rc\|dev'| head -1 | sed 's/.*tags\/\(.*\)$/\1/')

# build
CGO_ENABLED=0 TAGS="bindata postgres sqlite_unlock_notify" make build
cp gitea "${CURRENTDIR}/binout/"
